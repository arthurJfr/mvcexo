<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>GetBibli</title>
    <?php echo $view->add_webpack_style('admin'); ?>
</head>
<body>
<?php // $view->dump($view->getFlash()) ?>
<div class="bodywrap">
<aside id="masthead">

        <ul class="nav">
            <li class="nav__li"><a class="nav__h" href="<?= $view->path('home'); ?>"><h1 class="nav__title">GetBibli</h1></a>
                <div class="nav__underline"></div></li>


            <li class="nav__li"><a class="nav__a" href="<?= $view->path('home'); ?>">Home</a></li>


                <li class="nav__li"><a class="nav__a"  href="<?= $view->path('home-borrow'); ?>">Borrows</a>
                    </li>


            <li class="nav__li"><a class="nav__a"  href="<?= $view->path('home-products'); ?>">Products</a>
                </li>
  <li class="nav__li"><a class="nav__a"  href="<?= $view->path('home-sub'); ?>">Subscribers</a>
                </li>

        </ul>


</aside>


    <div class="container">
        <?= $content; ?>
    </div>
</div>


<?php echo $view->add_webpack_script('admin'); ?>
</body>
</html>
