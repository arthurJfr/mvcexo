<?php

namespace App\Controller;


use App\Model\BorrowModel;
use App\Model\ProductsModel;
use App\Model\SubscribersModel;
use App\Service\Form;
use App\Service\Validation;
use Core\Kernel\AbstractController;


class BorrowController extends AbstractController
{
    public function index()
    {
        $tablepdt = 'products';
        $allBorrows = BorrowModel::getAll();

        $this->render('app.borrow.index', array(
            'borrows' => $allBorrows
        ), 'admin');
    }
}